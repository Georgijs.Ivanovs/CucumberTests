Feature: Shopping in the store

  Scenario: User Opens Shops Main Page
    Given User is on the Home Page
    And User Selects Summer Dresses From Tom Menu
    And User Filters Dresses By Colour Yellow
    And User Sets Price Range From 25 To 30 Dollars
    Then All Dresses Are Yellow And Price Is Correct
    Then Home Page Logo Is Visible

