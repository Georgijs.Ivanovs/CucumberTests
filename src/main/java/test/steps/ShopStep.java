package test.steps;

import com.codeborne.selenide.*;
import config.MyWebDriver;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import java.util.Collection;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class ShopStep {

    public ShopStep() {
        new MyWebDriver();
    }

    @Given("^User is on the Home Page$")
    public void user_is_on_the_Home_Page() {
        open("http://automationpractice.com");
    }

    @Then("^Home Page Logo Is Visible$")
    public void homePageLogoIsVisible() {
        shopLogo().shouldBe(visible);
    }

    @And("^User Selects Summer Dresses From Tom Menu$")
    public void userSelectsSummerDressesFromTomMenu() {
        Actions action = new Actions(getWebDriver());
        action.moveToElement(topMenuDressesOption().hover()).moveToElement(dressesSubmenuSummerOption()).click().build().perform();
    }

    @And("^User Filters Dresses By Colour Yellow$")
    public void userFiltersDressesByColourYellow() {
        filterYellow().click();
    }

    @And("^User Sets Price Range From (\\d+) To (\\d+) Dollars$")
    public void userSelectsPriceFromToDollars(int arg0, int arg1) {
        adjustMinPriceTo(arg0);
        adjustMaxPriceTo(arg1);
        loadingAnimation().waitUntil(disappears,30000);
    }

    @Then("^All Dresses Are Yellow And Price Is Correct$")
    public void allDressesAreYellowAndPriceIsCorrect() {
        Collection<SelenideElement> collection = allFoundProducts();
        for(int i=1;i <= collection.size();i++){
            SelenideElement element = collection.iterator().next();
            element.$(colourYellow()).shouldBe(visible);
        }
    }

    private SelenideElement shopLogo() {
        return $(byCssSelector("img[alt='My Store']"));
    }

    private SelenideElement topMenu() {
        return $(byId("block_top_menu"));
    }

    private SelenideElement topMenuDressesOption() {
        return topMenu().$$(byText("Dresses")).filterBy(visible).first();
    }

    private SelenideElement dressesSubmenuSummerOption() {
        return $$(byAttribute("title", "Summer Dresses")).filterBy(visible).first();
    }

    private SelenideElement filterYellow() {
        return $(byId("ul_layered_id_attribute_group_3")).$(byText("Yellow"));
    }

    private SelenideElement priceSlider() {
        return $(byId("layered_price_slider"));
    }

    private SelenideElement priceSliderLeftKnob() {
        return priceSlider().$$(byCssSelector("a[href='#']")).first();
    }

    private SelenideElement priceSliderRightKnob() {
        return priceSlider().$$(byCssSelector("a[href='#']")).get(1);
    }

    private String getCurrentPriceRange(){
        return $(byId("layered_price_range")).getText();
    }

    private String[] splitToTwoPrices(){
        return getCurrentPriceRange().split("\\$");
    }

    private int getCurrentMinPrice(){
        String price[] = splitToTwoPrices();
        return Integer.parseInt(price[1].substring(0,2));
    }

    private int getCurrentMaxPrice(){
        String price[] = splitToTwoPrices();
        return Integer.parseInt(price[2].substring(0,2));
    }

    private SelenideElement loadingAnimation(){
        return $(byCssSelector("img[src='http://automationpractice.com/img/loader.gif']"));
    }

    private void adjustMinPriceTo(int price){
        Actions action = new Actions(getWebDriver());
        while (getCurrentMinPrice()<price){
            priceSliderLeftKnob().click();
            action.sendKeys(Keys.ARROW_RIGHT).build().perform();
        }
    }

    private SelenideElement foundProductsGrid(){
        return $(byCssSelector("ul[class='product_list grid row']"));
    }

    private Collection<SelenideElement> allFoundProducts(){
        return foundProductsGrid().$$(byCssSelector("div[class='product-container']"));
    }

    private By colourYellow(){
        return byId("color_19");
    }

    private void adjustMaxPriceTo(int price){
        Actions action = new Actions(getWebDriver());
        while (getCurrentMaxPrice()>price){
            priceSliderRightKnob().click();
            action.sendKeys(Keys.ARROW_LEFT).build().perform();
        }
    }
}